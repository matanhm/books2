<h1>This is your Books list</h1>

<table>
<tr>
    <th>Title</th> 
    <th>Author</th>
  </tr>
  <tr>
  @foreach($books as $book)
    <td>{{$book->title}}</td> 
    <td>{{$book->author}}</td>
  </tr>
  @endforeach
</table>