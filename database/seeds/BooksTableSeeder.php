<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
[
	[
            'title' => 'Harry Potter',
            'author' => 'J.K.Rolling',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],
    [
            'title' => 'Games of Thornes',
            'author' => 'Gorge RR Marting',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
    ],
        ]);
    }
}
